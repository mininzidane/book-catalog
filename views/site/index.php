<?php

/**
 * @var                      $this yii\web\View
 * @var \app\models\Author[] $authors
 */

$this->title = 'My Yii Application';
?>

<h2>Список авторов</h2>
<ul>
	<?php foreach ($authors as $author) { ?>
		<li>
			<a href="<?= \yii\helpers\Url::to(['author', 'id' => $author->id]) ?>"><?= $author->fio ?></a>
		</li>
	<?php } ?>
</ul>