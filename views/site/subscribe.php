<?php

use yii\widgets\ActiveForm;

/**
 * @var                           $this yii\web\View
 * @var \app\models\Subscribe     $model
 */

$this->title = 'Подписка';
?>

<?php if (Yii::$app->session->getFlash('subscribe') === true) { ?>
	<div class="alert alert-success">Вы подписаны!</div>
<?php } else { ?>
	<div class="row">
		<div class="col-sm-6">
			<?php $form = ActiveForm::begin(); ?>
			<?= $form->field($model, 'phone')->textInput() ?>
			<div class="form-group">
				<?= \yii\helpers\Html::submitButton('Подписаться', ['class' => 'btn btn-success']) ?>
			</div>
			<?php ActiveForm::end(); ?>
		</div>
	</div>
<?php } ?>
