<?php
/**
 * @var \app\models\Author[] $authors
 */
?>

<h2>Топ авторов:</h2>
<table class="table">
	<tr>
		<th>ID</th>
		<th>ФИО</th>
		<th>Количество книг</th>
	</tr>
<?php foreach ($authors as $author) { ?>
	<tr>
		<td><?= $author->id ?></td>
		<td><?= $author->fio ?></td>
		<td><?= $author->cnt ?></td>
	</tr>
<?php } ?>
</table>