<?php

/**
 * @var                              $this yii\web\View
 * @var \app\models\Author           $author
 * @var \yii\data\ActiveDataProvider $provider
 */

$this->title = 'Author';
?>

	<h2>Список книг</h2>
	<p>
		<?php if (!\Yii::$app->user->isGuest) { ?>
			<a href="<?= \yii\helpers\Url::to(['book/create']) ?>" class="btn btn-success">Добавить книгу</a>
		<?php } ?>
		<a href="<?= \yii\helpers\Url::to(['site/subscribe', 'id' => $author->id]) ?>"
		   class="btn btn-info">Подписаться</a>
	</p>

<?php
$columns = [
	'id',
	'title',
	'year',
	'desc',
	'isbn',
	'photo',
];
if (!\Yii::$app->user->isGuest) {
	$columns[] = [
		'class'      => \yii\grid\ActionColumn::className(),
		'controller' => 'book',
	];
}
echo \yii\grid\GridView::widget([
	'dataProvider' => $provider,
	'columns'      => $columns,
]) ?>