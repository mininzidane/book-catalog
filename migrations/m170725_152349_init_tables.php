<?php

use yii\db\Migration;

class m170725_152349_init_tables extends Migration
{
	public function safeUp()
	{
		$this->createTable('book', [
			'id'    => $this->primaryKey(),
			'title' => $this->char(255)->notNull(),
			'year'  => $this->char(4)->null(),
			'desc'  => $this->text()->null(),
			'isbn'  => $this->integer()->null(),
			'photo' => $this->char(255)->null(),
		]);
		$this->createTable('author', [
			'id'  => $this->primaryKey(),
			'fio' => $this->char(128)->notNull(),
		]);
		$this->createTable('book_author', [
			'bookId'   => $this->integer()->notNull(),
			'authorId' => $this->integer()->notNull(),
		]);
		$this->createTable('subscribe', [
			'id'       => $this->primaryKey(),
			'phone'    => $this->char(11)->notNull(),
			'authorId' => $this->integer()->notNull(),
		]);
	}

	public function safeDown()
	{
		$this->dropTable('subscribe');
		$this->dropTable('book_author');
		$this->dropTable('author');
		$this->dropTable('book');
	}
}
