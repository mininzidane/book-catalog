<?php

$faker = Faker\Factory::create();

ini_set('memory_limit', '-1');
return [
	'fio' => $faker->firstName . ' ' . $faker->lastName,
];