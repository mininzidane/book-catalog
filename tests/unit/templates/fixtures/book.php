<?php

$faker = Faker\Factory::create();

ini_set('memory_limit', '-1');
return [
	'title'    => $faker->sentence(3),
	'year'     => $faker->numberBetween(1990, 1996),
	'desc'     => $faker->realText(20),
	'isbn'     => $faker->numberBetween(),
];