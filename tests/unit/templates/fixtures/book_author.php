<?php

$faker = Faker\Factory::create();

ini_set('memory_limit', '-1');
return [
	'bookId'   => $faker->numberBetween(1, 100),
	'authorId' => $faker->numberBetween(1, 30),
];