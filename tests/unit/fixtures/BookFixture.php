<?php

namespace tests\unit\fixtures;

class BookFixture extends \yii\test\ActiveFixture
{
	public $modelClass = 'app\models\Book';
}
