<?php

namespace tests\unit\fixtures;

class BookAuthorFixture extends \yii\test\ActiveFixture
{
	public $modelClass = 'app\models\BookAuthor';
}