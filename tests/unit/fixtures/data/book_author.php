<?php

return [
    [
        'bookId' => 56,
        'authorId' => 17,
    ],
    [
        'bookId' => 66,
        'authorId' => 8,
    ],
    [
        'bookId' => 89,
        'authorId' => 21,
    ],
    [
        'bookId' => 100,
        'authorId' => 19,
    ],
    [
        'bookId' => 52,
        'authorId' => 23,
    ],
    [
        'bookId' => 11,
        'authorId' => 29,
    ],
    [
        'bookId' => 63,
        'authorId' => 1,
    ],
    [
        'bookId' => 8,
        'authorId' => 13,
    ],
    [
        'bookId' => 94,
        'authorId' => 1,
    ],
    [
        'bookId' => 8,
        'authorId' => 2,
    ],
    [
        'bookId' => 99,
        'authorId' => 6,
    ],
    [
        'bookId' => 53,
        'authorId' => 1,
    ],
    [
        'bookId' => 80,
        'authorId' => 19,
    ],
    [
        'bookId' => 86,
        'authorId' => 8,
    ],
    [
        'bookId' => 13,
        'authorId' => 11,
    ],
    [
        'bookId' => 85,
        'authorId' => 14,
    ],
    [
        'bookId' => 23,
        'authorId' => 17,
    ],
    [
        'bookId' => 15,
        'authorId' => 22,
    ],
    [
        'bookId' => 97,
        'authorId' => 11,
    ],
    [
        'bookId' => 88,
        'authorId' => 17,
    ],
    [
        'bookId' => 48,
        'authorId' => 22,
    ],
    [
        'bookId' => 8,
        'authorId' => 3,
    ],
    [
        'bookId' => 53,
        'authorId' => 18,
    ],
    [
        'bookId' => 45,
        'authorId' => 2,
    ],
    [
        'bookId' => 42,
        'authorId' => 19,
    ],
    [
        'bookId' => 10,
        'authorId' => 22,
    ],
    [
        'bookId' => 92,
        'authorId' => 10,
    ],
    [
        'bookId' => 70,
        'authorId' => 5,
    ],
    [
        'bookId' => 32,
        'authorId' => 5,
    ],
    [
        'bookId' => 41,
        'authorId' => 30,
    ],
    [
        'bookId' => 42,
        'authorId' => 14,
    ],
    [
        'bookId' => 76,
        'authorId' => 10,
    ],
    [
        'bookId' => 34,
        'authorId' => 24,
    ],
    [
        'bookId' => 22,
        'authorId' => 1,
    ],
    [
        'bookId' => 38,
        'authorId' => 8,
    ],
    [
        'bookId' => 35,
        'authorId' => 23,
    ],
    [
        'bookId' => 87,
        'authorId' => 14,
    ],
    [
        'bookId' => 24,
        'authorId' => 11,
    ],
    [
        'bookId' => 95,
        'authorId' => 11,
    ],
    [
        'bookId' => 20,
        'authorId' => 2,
    ],
    [
        'bookId' => 100,
        'authorId' => 3,
    ],
    [
        'bookId' => 85,
        'authorId' => 26,
    ],
    [
        'bookId' => 66,
        'authorId' => 19,
    ],
    [
        'bookId' => 76,
        'authorId' => 1,
    ],
    [
        'bookId' => 58,
        'authorId' => 21,
    ],
    [
        'bookId' => 11,
        'authorId' => 13,
    ],
    [
        'bookId' => 74,
        'authorId' => 2,
    ],
    [
        'bookId' => 95,
        'authorId' => 25,
    ],
    [
        'bookId' => 2,
        'authorId' => 24,
    ],
    [
        'bookId' => 39,
        'authorId' => 11,
    ],
    [
        'bookId' => 71,
        'authorId' => 11,
    ],
    [
        'bookId' => 10,
        'authorId' => 16,
    ],
    [
        'bookId' => 51,
        'authorId' => 11,
    ],
    [
        'bookId' => 74,
        'authorId' => 23,
    ],
    [
        'bookId' => 85,
        'authorId' => 3,
    ],
    [
        'bookId' => 71,
        'authorId' => 13,
    ],
    [
        'bookId' => 4,
        'authorId' => 22,
    ],
    [
        'bookId' => 41,
        'authorId' => 12,
    ],
    [
        'bookId' => 27,
        'authorId' => 13,
    ],
    [
        'bookId' => 38,
        'authorId' => 2,
    ],
    [
        'bookId' => 7,
        'authorId' => 29,
    ],
    [
        'bookId' => 16,
        'authorId' => 10,
    ],
    [
        'bookId' => 39,
        'authorId' => 21,
    ],
    [
        'bookId' => 13,
        'authorId' => 30,
    ],
    [
        'bookId' => 80,
        'authorId' => 16,
    ],
    [
        'bookId' => 14,
        'authorId' => 21,
    ],
    [
        'bookId' => 35,
        'authorId' => 30,
    ],
    [
        'bookId' => 24,
        'authorId' => 16,
    ],
    [
        'bookId' => 3,
        'authorId' => 28,
    ],
    [
        'bookId' => 98,
        'authorId' => 10,
    ],
    [
        'bookId' => 12,
        'authorId' => 20,
    ],
    [
        'bookId' => 8,
        'authorId' => 27,
    ],
    [
        'bookId' => 93,
        'authorId' => 7,
    ],
    [
        'bookId' => 97,
        'authorId' => 28,
    ],
    [
        'bookId' => 78,
        'authorId' => 5,
    ],
    [
        'bookId' => 96,
        'authorId' => 11,
    ],
    [
        'bookId' => 52,
        'authorId' => 21,
    ],
    [
        'bookId' => 76,
        'authorId' => 14,
    ],
    [
        'bookId' => 8,
        'authorId' => 2,
    ],
    [
        'bookId' => 31,
        'authorId' => 15,
    ],
    [
        'bookId' => 67,
        'authorId' => 3,
    ],
    [
        'bookId' => 11,
        'authorId' => 19,
    ],
    [
        'bookId' => 3,
        'authorId' => 12,
    ],
    [
        'bookId' => 60,
        'authorId' => 12,
    ],
    [
        'bookId' => 57,
        'authorId' => 29,
    ],
    [
        'bookId' => 92,
        'authorId' => 5,
    ],
    [
        'bookId' => 15,
        'authorId' => 6,
    ],
    [
        'bookId' => 76,
        'authorId' => 21,
    ],
    [
        'bookId' => 68,
        'authorId' => 4,
    ],
    [
        'bookId' => 20,
        'authorId' => 27,
    ],
    [
        'bookId' => 89,
        'authorId' => 13,
    ],
    [
        'bookId' => 42,
        'authorId' => 9,
    ],
    [
        'bookId' => 53,
        'authorId' => 9,
    ],
    [
        'bookId' => 43,
        'authorId' => 13,
    ],
    [
        'bookId' => 7,
        'authorId' => 17,
    ],
    [
        'bookId' => 78,
        'authorId' => 28,
    ],
    [
        'bookId' => 73,
        'authorId' => 25,
    ],
    [
        'bookId' => 34,
        'authorId' => 24,
    ],
    [
        'bookId' => 88,
        'authorId' => 20,
    ],
    [
        'bookId' => 94,
        'authorId' => 10,
    ],
];
