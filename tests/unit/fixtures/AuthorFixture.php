<?php

namespace tests\unit\fixtures;

class AuthorFixture extends \yii\test\ActiveFixture
{
	public $modelClass = 'app\models\Author';
}
