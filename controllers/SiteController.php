<?php

namespace app\controllers;

use app\models\Author;
use app\models\Subscribe;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;

class SiteController extends Controller
{

	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					['allow' => true, 'actions' => ['login', 'logout', 'index', 'author', 'report', 'subscribe']],
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
			'verbs'  => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'logout' => ['post'],
				],
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function actions()
	{
		return [
			'error'   => [
				'class' => 'yii\web\ErrorAction',
			],
			'captcha' => [
				'class'           => 'yii\captcha\CaptchaAction',
				'fixedVerifyCode' => YII_ENV_TEST? 'testme': null,
			],
		];
	}

	/**
	 * Displays homepage.
	 * @return string
	 */
	public function actionIndex()
	{
		$authors = Author::find()->all();
		return $this->render('index', compact('authors'));
	}

	public function actionAuthor($id)
	{
		if (!$author = Author::findOne($id)) {
			throw new NotFoundHttpException('Author not found');
		}

		$provider = new ActiveDataProvider([
			'query'      => $author->getBooks(),
			'pagination' => [
				'pageSize' => 50,
			],
			'sort'       => [
				'defaultOrder' => [
					'isbn' => SORT_DESC,
				],
			],
		]);
		return $this->render('author', compact('provider', 'author'));
	}

	/**
	 * Login action.
	 * @return Response|string
	 */
	public function actionLogin()
	{
		if (!Yii::$app->user->isGuest) {
			return $this->goHome();
		}

		$model = new LoginForm();
		if ($model->load(Yii::$app->request->post()) && $model->login()) {
			return $this->goBack();
		}
		return $this->render('login', [
			'model' => $model,
		]);
	}

	/**
	 * Logout action.
	 * @return Response
	 */
	public function actionLogout()
	{
		Yii::$app->user->logout();

		return $this->goHome();
	}

	public function actionReport($year)
	{
		$authors = Author::getTopOfTheYear($year);
		return $this->render('report', compact('authors'));
	}

	public function actionSubscribe($id)
	{
		$model = new Subscribe(['authorId' => $id]);
		if ($model->load(Yii::$app->request->post()) && $model->subscribe()) {
			Yii::$app->session->setFlash('subscribe');
		}
		return $this->render('subscribe', compact('model'));
	}
}
