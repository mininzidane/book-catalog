<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * Class Author
 * @package app\models
 * @property int    $id
 * @property string $fio
 * @property Book[] $books
 */
class Author extends ActiveRecord
{
	public $cnt;

	public static function getTopOfTheYear($year, $count = 10)
	{
		$authors = self::find()
			->select(['COUNT(*) AS cnt', 'a.*'])
			->alias('a')
			->innerJoin(BookAuthor::tableName() . ' ba', 'ba.authorId = a.id')
			->innerJoin(Book::tableName() . ' b', 'b.id = ba.bookId AND b.year = :year', ['year' => $year])
			->groupBy('a.id')
			->orderBy(['cnt' => SORT_DESC])
			->limit($count)
			->all();
		return $authors;
	}

	public function getBookAuthor()
	{
		return $this->hasMany(BookAuthor::className(), ['authorId' => 'id']);
	}

	public function getBooks()
	{
		return $this->hasMany(Book::className(), ['id' => 'bookId'])->via('bookAuthor');
	}
}
