<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\db\Exception;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
 * Class Book
 * @package app\models
 * @property int      $id
 * @property string   $title
 * @property string   $year
 * @property string   $desc
 * @property int      $isbn
 * @property string   $photo
 * @property Author[] $authors
 */
class Book extends ActiveRecord
{
	public $authorIds = [];
	/** @var UploadedFile */
	public $imageFile;

	public function getBookAuthor()
	{
		return $this->hasMany(BookAuthor::className(), ['bookId' => 'id']);
	}

	public function getAuthors()
	{
		return $this->hasMany(Author::className(), ['id' => 'authorId'])->via('bookAuthor');
	}

	public function rules()
	{
		return [
			[['title'], 'required'],
			[['imageFile'], 'file', 'extensions' => 'png, jpg'],
			[['year', 'desc', 'isbn', 'photo', 'authorIds'], 'safe'],
		];
	}

	public function getUploadsPath()
	{
		return \Yii::getAlias('@webroot') . DIRECTORY_SEPARATOR . 'uploads';
	}

	public function upload()
	{
		if (!file_exists($this->getUploadsPath())) {
			mkdir($this->getUploadsPath());
		}
		$filename = 'uploads/' . $this->imageFile->baseName . '.' . $this->imageFile->extension;
		if (!$this->imageFile->saveAs($filename)) {
			return false;
		}
		$this->photo = '/' . $filename;
		return true;
	}

	public function afterFind()
	{
		$this->authorIds = ArrayHelper::getColumn($this->authors, 'id');
		return parent::afterFind();
	}

	public function afterSave($insert, $changedAttributes)
	{
		if (is_array($this->authorIds)) {
			foreach ($this->authorIds as $authorId) {
				$config = [
					'bookId'   => $this->id,
					'authorId' => $authorId,
				];
				if (!BookAuthor::findOne($config)) {
					if (!(new BookAuthor($config))->save()) {
						throw new Exception('Could not save relation');
					}
				}
			}
		}

		return parent::afterSave($insert, $changedAttributes);
	}

	public function beforeSave($insert)
	{
		if (!parent::beforeSave($insert)) {
			return false;
		}
		if ($this->isNewRecord) {
			Subscribe::sendNotifications($this->authorIds);
		}
		return true;
	}

}
