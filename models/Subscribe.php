<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * @package app\models
 * @property string $phone
 * @property int    $authorId
 * @property Author $author
 */
class Subscribe extends ActiveRecord
{
	public function getAuthor()
	{
		return $this->hasOne(Author::className(), ['id' => 'authorId']);
	}

	/**
	 * @return array the validation rules.
	 */
	public function rules()
	{
		return [
			[['phone', 'authorId'], 'required'],
			[['phone'], 'integer'],
			[['phone'], 'string', 'length' => 11],
		];
	}

	public function subscribe()
	{
		$condition = ['phone' => $this->phone, 'authorId' => $this->authorId];
		if (!self::find()->where($condition)->one()) {
			$model = new self($condition);
			return $model->save();
		}
		return true;
	}

	public static function sendNotifications($authorIds = [])
	{
		/** @var Subscribe[] $models */
		$models = self::find()->with('author')->where(['authorId' => $authorIds])->all();
		$texts = [];
		foreach ($models as $model) {
			// здесь бы мы слали СМС на номера, а так просто демонстрация
			$texts[] = "Шлем смс на номер {$model->phone}, что у автора {$model->author->fio} вышла новая книга" . PHP_EOL;
		}
		return $texts;
	}
}
