<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * @package app\models
 * @property int $bookId
 * @property int $authorId
 */
class BookAuthor extends ActiveRecord
{
	public static function tableName()
	{
		return 'book_author';
	}

	public function rules()
	{
		return [
			[['bookId', 'authorId'], 'required'],
		];
	}
}
